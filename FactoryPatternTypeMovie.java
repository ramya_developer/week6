package com.greatlearning.movies;

import com.greatlearning.movieslist.MoviesComingSoon; // importing the classes that are created in movies list package
import com.greatlearning.movieslist.TheatricalRelease; // importing the classes that are created in movies list package
import com.greatlearning.movieslist.TopRatedMovies; // importing the classes that are created in movies list package
import com.greatlearning.movieslist.TopRatedMoviesInIndia; // importing the classes that are created in movies list package

// creating FactoryPatternTypeMovie class

public class FactoryPatternTypeMovie {
	public IMovies getMovies(String movieType) {
//using if-else condition
		if (movieType == null) {
			return null;
		}
		if (movieType.equalsIgnoreCase("MOVIES_COMING")) {
			 return  new MoviesComingSoon();
		}
		else if (movieType.equalsIgnoreCase("MOVIES_IN_THEATRES")) {
			return new TheatricalRelease();
		} 
		else if (movieType.equalsIgnoreCase("TOP_RATED_INDIA")) {
			return new TopRatedMoviesInIndia();
		} 
		else if (movieType.equalsIgnoreCase("TOP_RATED_MOVIES")) {
			return new TopRatedMovies();
		}
		return null;
}
}