package com.greatlearning;
//creating Main class

import com.greatlearning.movies.FactoryPatternTypeMovie; // importing the classes that are created in movies package
import com.greatlearning.movies.IMovies;      // importing the classes that are created in movies package

public class Main {
	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub

		FactoryPatternTypeMovie movieFactory = new FactoryPatternTypeMovie();
		
	      //get an object of Circle and call its draw method.
		
	     IMovies ComingSoonMovies = movieFactory.getMovies("MOVIES_COMING");
	   //call draw method of Circle
	       ComingSoonMovies.display();
	       
	       
	      IMovies TheatricalMovies = movieFactory.getMovies("MOVIES_IN_THEATRES");
		
	      TheatricalMovies.display();
	      
	      
	    IMovies TopRatedMoviesInIndia = movieFactory.getMovies("TOP_RATED_INDIA");
			
	    TopRatedMoviesInIndia.display();
	      
	    IMovies TopRatedMovies = movieFactory.getMovies("TOP_RATED_MOVIES");
			
	    TopRatedMovies.display();
}
}